//requerimos el módulo para conectarse a la base de datos


const mysql = require('mysql')
//requerimos el archivo donde tenemos configurada la conexion
const conn = require('../config/conn')

//creamos la constante a ser exportada
const clientes = {

    //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
    async getClientes () {

        //Guardamos en una variable la consulta que queremos generar
        let sql = 'SELECT * FROM clientes'
        //Con el archivo de conexion a la base, enviamos la consulta a la misma
        //Ponemos un await porque desconocemos la demora de la misma
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },

    async getClienteById (id) {

        let sql = 'SELECT * FROM clientes WHERE id = ' + id
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },

    async postCliente(nuevo) {

        let sql = "INSERT INTO `test`.`clientes` ( `id` ,`name` ,`direccion` ,`telefono` ,`foto` ,`celular` ,`correo`)VALUES (NULL , '"+ nuevo.name + "', '"+ nuevo.direccion + "', '"+ nuevo.telefono + "', '"+ nuevo.foto + "', '"+ nuevo.celular + "', '"+ nuevo.correo + "');"
           
        let resultado = await conn.query(sql)
        let response = {error: "usuario creado"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },
}
//Exportamos el módulo
module.exports = clientes
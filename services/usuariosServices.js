//requerimos el módulo para conectarse a la base de datos
const mysql = require('mysql')
//requerimos el archivo donde tenemos configurada la conexion
const conn = require('../config/conn')
//creamos la constante a ser exportada
const usuarios = {
    async postUsuario(nuevo) {

        let sql = "INSERT INTO `ecommerce`.`usuarios` ( `id` , `nombre`, `email` ,`password`, `rol`)VALUES (NULL , '"+ nuevo.nombre + "', '"+ nuevo.email + "', '"+ nuevo.password +"', '"+ nuevo.rol + "');"
       console.log(sql)
        let resultado = await conn.query(sql)
        let response = {error: "usuario creado"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },
        //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
        async getUsuarios () {

            //Guardamos en una variable la consulta que queremos generar
            let sql = 'SELECT * FROM usuarios'
            //Con el archivo de conexion a la base, enviamos la consulta a la misma
            //Ponemos un await porque desconocemos la demora de la misma
            let resultado = await conn.query(sql)
            let response = {error: "No se encontraron registros"}
            if(resultado.code) {
                response = {error: "Error en la consulta SQL"}
            }else if (resultado.length > 0) {
                response = {result: resultado}
            }
            return response
        },
    
        async getUsuarioById (id) {
    
            let sql = 'SELECT * FROM usuarios WHERE id = ' + id
            let resultado = await conn.query(sql)
            let response = {error: "No se encontraron registros"}
            if(resultado.code) {
                response = {error: "Error en la consulta SQL"}
            }else if (resultado.length > 0) {
                response = {result: resultado}
            }
            return response
        },

        async putUsuario(nuevo) {
            let sql = "UPDATE `usuarios` SET `nombre`='"+ nuevo.nombre + "', `email`='"+ nuevo.email + "', `password`='"+ nuevo.password + "', `rol`='"+ nuevo.rol + "' WHERE `id` = '"+ nuevo.id + "'"
            
            console.log(sql)
            let resultado = await conn.query(sql)
            let response = {error: "usuario modificado"}
            if(resultado.code) {
                response = {error: "Error en la consulta SQL"}
            }else if (resultado.length > 0) {
                response = {result: resultado}
            }
            return response
        },
    
}
//Exportamos el módulo
module.exports = usuarios
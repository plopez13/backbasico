# BackBasico

//Necesitas crear un archivo .env con los siguiente datos

SECRET = ''                     //clave de 32 caracteres
DB_PORT   = "3306"              //puerto de base de datos
DB_HOST     = "localhost"       // url de base de datos
DB_USER     = "root"            //usuario de base de datos
DB_PASSWORD = ""                //password de usuario de base de datos 
DB_DATABASE = "ecommerce"       // base en uso
PORT = "3000"                   // puerto de ejecuciòn de back end
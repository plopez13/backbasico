const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require("dotenv").config();

process.env.SECRET = process.env.SECRET;
const secret = process.env.SECRET;
const port = process.env.PORT

// Configuración de CORS
app.use(cors());

// Controladores de las rutas
require("./controllers/loginController")(app);
require("./controllers/clientesController")(app);
require("./controllers/usuariosController")(app);
require("./controllers/ventasController")(app);

app.listen(port, function () {
    console.log("Servidor iniciado en el puerto " + port);
});